#include "Zbozi.h"
#include <string>

Zbozi::Zbozi(string nazevPredmeta) {
	m_nazevPredmeta = nazevPredmeta;
}

string Zbozi::getNazevPredmeta() {
	return m_nazevPredmeta;
}

void Zbozi::printInfo() {
	cout << "Zbozi: " << getNazevPredmeta() << endl;
}
