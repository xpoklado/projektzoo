#include "VypisInfoHrace.h"

VypisInfoHrace::VypisInfoHrace(string popis):PolozkaMenu(popis) {
}

void VypisInfoHrace::provedPolozku(Hrac* hrac, Inventar* inventar, Zbozi* zbozi) {
	hrac->printInfo(inventar, zbozi );
}
