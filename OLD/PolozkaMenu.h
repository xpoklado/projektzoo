#ifndef POLOZKAMENU_H
#define POLOZKAMENU_H
#include <iostream>
#include "Hrac.h"
using namespace std;

class PolozkaMenu {

private:
	string m_popisAkce;

public:
	PolozkaMenu(string popis);
	string getPopisPolozky();
	void provedPolozku(Hrac* hrac);
};
#endif