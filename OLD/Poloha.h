#ifndef POLOHA_H
#define POLOHA_H
#include <iostream>
#include "Hrac.h"
#include "MapaPole.h"
using namespace std;

class Poloha {

protected:
	string m_nazevPolohy;
	Hrac* m_hrac;

public:
	Poloha(string nazevPolohy);
	virtual void vytvorMenu(Hrac* hrac, MapaPole* menuPole) = 0;
	virtual string getNazevPolohy() = 0;
	virtual Hrac* getHrace() = 0;
	virtual void setHrac(Hrac* hrac) = 0;
	virtual void printInfo() = 0;
};
#endif