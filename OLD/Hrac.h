#ifndef HRAC_H
#define HRAC_H
#include <iostream>
#include "Inventar.h"
#include "Zbozi.h"
using namespace std;

class Hrac {

private:
	string m_jmeno;
	int m_sila;
	int m_zdravi;
	int m_penize;
	Inventar* m_inventar;

public:
	Hrac(string jmeno);
	string getJmeno();
	Inventar* getInventar();
	int getSila();
	int getZdravi();
	int getPenize();
	void setJmeno(string jmeno);
	void setPenize(int penize);
	void setZdravi(int zdravi);
	void setSila(int sila);
	void pridejPenize(int castka);
	void odeberPenize(int castka);
	void printInfo(Inventar* inventar, Zbozi* zbozi);
};
#endif