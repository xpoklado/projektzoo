#ifndef INVENTAR_H
#define INVENTAR_H
#include <iostream>
#include <vector>
#include "Zbozi.h"
using namespace std;

class Inventar {

private:
	int m_maxPocetPredmetu;
	vector<Zbozi*> m_zbozi;

public:
	Inventar(int maxPocetPredmetu);
	int getMaxPocetPredmetu();
	int getPocetZbozi();
	Zbozi* getZbozi(int poradi);
	void pridejZbozi(Zbozi* zbozi);
	void odeberZbozi(int poradi);
	void printInfo();
};
#endif