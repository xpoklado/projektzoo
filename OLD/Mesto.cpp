#include "Mesto.h"
#include <string>

Mesto::Mesto(string nazevPolohy, int cenaN, int cenaP): Poloha(nazevPolohy) {
	m_nazevPolohy = nazevPolohy;
	m_cenaN = cenaN;
	m_cenaP = cenaP;
}

Hrac* Mesto::getHrace() {
	return m_hrac;
}

string Mesto::getNazevPolohy() {
	return m_nazevPolohy;
}

int Mesto::getCenaN() {
	return m_cenaN;
}

int Mesto::getCenaP() {
	return m_cenaP;
}

void Mesto::setHrac(Hrac* hrac) {
	m_hrac = hrac;
}

void Mesto::odeberZbozi(int poradi) {
	m_zbozi.erase(m_zbozi.at[poradi]);
}

void Mesto::prodejZbozi(int poradi) {
	odeberZbozi(poradi);
	getHrace()->odeberPenize(m_cenaP);
	getHrace()->getInventar()->odeberZbozi(poradi);
}

void Mesto::pridejZbozi(Zbozi* zbozi) {
	m_zbozi.push_back(zbozi);
}

void Mesto::nakupZbozi(Zbozi* zbozi) {
	pridejZbozi(zbozi);
	getHrace()->pridejPenize(m_cenaN);
	getHrace()->getInventar()->pridejZbozi(zbozi);
}

void Mesto::printInfo() {
	cout << "Nazev mesta je: "<<m_nazevPolohy<<"V obchode tu je "<< m_zbozi.at(0)->getNazevPredmeta()<<". Cena nakupu je: "<<m_cenaN<<", cena prodeje je: "<<m_cenaP<< endl;
}

void Mesto::vytvorMenuM(Hrac * hrac, MapaPole * mapaPole){
	getHrace()->setSila((hrac->getSila) + 5);

	cout << "Vy jste ve meste. Muzete tu: ";
	cout << "1. Nakoupit zbozi\n";
	cout << "2. Prodat zbozi\n";
	cout << "3. Dozvedet info o lokaci\n";
	cout << "4. Zvolit si dalsi lokace\n";
	cout << "Vase volba: ";
	int volba;
	cin >> volba;
	switch (volba) {
	case 1:
		nakupZbozi(m_zbozi.at(0));
	case 2:
		prodejZbozi(0);
	case 3:
		printInfo();
	case 4:
		mapaPole->vytvorMenu(hrac, mapaPole);
}
