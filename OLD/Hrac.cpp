#include "Hrac.h"
#include <string>

Hrac::Hrac(string jmeno) {
	m_jmeno = jmeno;
	m_sila = 70;
	m_zdravi = 120;
	m_penize = 500;
}

string Hrac::getJmeno() {
	return m_jmeno;
}

Inventar* Hrac::getInventar() {
	return m_inventar;
}

int Hrac::getSila() {
	return m_sila;
}

int Hrac::getZdravi() {
	return m_zdravi;
}

int Hrac::getPenize() {
	return m_penize;
}

void Hrac::setJmeno(string jmeno) {
	m_jmeno = jmeno;
}

void Hrac::setPenize(int penize) {
	m_penize = penize;
}

void Hrac::setZdravi(int zdravi) {
	m_zdravi = zdravi;
}

void Hrac::setSila(int sila) {
	m_sila = sila;
}

void Hrac::pridejPenize(int castka) {
	m_penize += castka;
}

void Hrac::odeberPenize(int castka) {
	m_penize -= castka;
}

void Hrac::printInfo(Inventar* inventar, Zbozi* zbozi) {
	cout << "Ahoj, jmenuju se " << m_jmeno << ". Mam " << m_penize << " penez, " << m_zdravi << " zdravi a " << m_sila << " sily." << endl;
	inventar->printInfo();
}
