#ifndef HRA_H
#define HRA_H
#include <iostream>
#include <vector>
#include "Hrac.h"
#include "UlozVysledky.h"
#include "VypisRejestrik.h"
#include "VypisInfoHrace.h"
#include "ZvolSiCestu.h"
#include "Inventar.h"
#include "MapaPole.h"
#include "PolozkaMenu.h"
using namespace std;

class Hra {

private:
	vector<Hrac*> m_hrac;
	vector<PolozkaMenu*> m_menu;
	vector<MapaPole*> m_mapaPole;

public:
	Hra();
	void vytvorHrace();
	void vytvorMenu();
	void chciNecoUdelat();
	~Hra();
};
#endif