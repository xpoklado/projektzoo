#ifndef ZBOZI_H
#define ZBOZI_H
#include <iostream>
using namespace std;

class Zbozi {

private:
	string m_nazevPredmeta;

public:
	Zbozi(string nazevPredmeta);
	string getNazevPredmeta();
	void printInfo();
};
#endif