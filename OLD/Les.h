#ifndef LES_H
#define LES_H
#include <iostream>
#include "Poloha.h"
#include "Loupeznici.h"
#include "Hrac.h"
#include "MapaPole.h"
using namespace std;

class Les :public Poloha {

public:
	Loupeznici* m_loupeznici;


	Les(string nazevPolohy);
	Hrac* getHrace();
	string getNazevPolohy();
	Loupeznici * getLoupezniky();
	void setHrac(Hrac* hrac);
	void bojuj(Hrac* hrac, Loupeznici* loupeznici);
	void printInfo();
	void vytvorMenu(Hrac * hrac, MapaPole * mapaPole);
	void vytvorLoupezniky();
};
#endif