#ifndef MAPAPOLE_H
#define MAPAPOLE_H
#include <iostream>
#include "Poloha.h"
#include "Trava.h"
#include "Les.h"
#include "Skala.h"
#include "Mesto.h"
#include "Loupeznici.h"
using namespace std;

class MapaPole {

private:
	vector<Poloha*> m_poloha;
	vector<Zbozi*> m_zbozi;

public:
	MapaPole();
	Poloha* getPoloha(int poradi);
	void vytvorMenu(Hrac * hrac, MapaPole * mapaPole);
};
#endif