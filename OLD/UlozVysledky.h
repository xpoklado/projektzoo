#ifndef ULOZVYSLEDKY_H
#define ULOZVYSLEDKY_H
#include <iostream>
#include "PolozkaMenu.h"
#include "Hrac.h"
using namespace std;

class UlozVysledky :public PolozkaMenu {


public:
	UlozVysledky(string popis);
	void provedPolozku(Hrac* hrac);
};
#endif