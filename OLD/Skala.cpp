#include "Skala.h"

Skala::Skala(string nazevPolohy): Poloha(nazevPolohy) {
}

string Skala::getNazevPolohy() {
	return m_nazevPolohy;
}

Hrac* Skala::getHrace() {
	return m_hrac;
}

void Skala::setHrac(Hrac* hrac) {
	m_hrac = hrac;
}

void Skala::printInfo() {
	cout << "Pri kazdem kroku na horach budete utracet svou silu. Ted vase sila je "<< getHrace()->getSila() << endl;
}

void Skala::vytvorMenu(Hrac* hrac, MapaPole* mapaPole) {
	getHrace()->setSila(hrac->getSila() - 3);

	cout << "Vy jste na horach. Muzete tu: ";
	cout << "1. Dozvedet info o lokaci\n";
	cout << "2. Zvolit si dalsi lokace\n";
	cout << "Vase volba: ";
	int volba;
	cin >> volba;
	switch (volba) {
	case 1:
		printInfo();
	case 2:
		mapaPole->vytvorMenu(hrac, mapaPole);
}
