#ifndef LOUPEZNICI_H
#define LOUPEZNICI_H
#include <iostream>
using namespace std;

class Loupeznici {

private:
	int m_LSila;

public:
	Loupeznici(int LSila);
	int getLSila();
	void printInfo();

};
#endif