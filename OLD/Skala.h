#ifndef SKALA_H
#define SKALA_H
#include <iostream>
#include "Poloha.h"
#include "Hrac.h"
#include "MapaPole.h"
using namespace std;

class Skala :public Poloha {


public:
	Skala(string nazevPolohy);
	string getNazevPolohy();
	Hrac* getHrace();
	void setHrac(Hrac* hrac);
	void printInfo();
	void vytvorMenu(Hrac* hrac, MapaPole* mapaPole);
};
#endif