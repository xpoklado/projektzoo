#ifndef VYPISREJESTRIK_H
#define VYPISREJESTRIK_H
#include <iostream>
#include "PolozkaMenu.h"
#include "Hrac.h"

class VypisRejestrik :public PolozkaMenu {


public:
	VypisRejestrik(string popis);
	void provedPolozku(Hrac * hrac);
};
#endif