#ifndef TRAVA_H
#define TRAVA_H
#include <iostream>
#include "Poloha.h"
#include "Hrac.h"
#include "MapaPole.h"
using namespace std;

class Trava :public Poloha {


public:
	Trava(string nazevPolohy);
	string getNazevPolohy();
	Hrac* getHrace();
	void setHrac(Hrac* hrac);
	void printInfo();
	void vytvorMenu(Hrac* hrac, MapaPole* mapaPole);
};
#endif