#include "VypisRejestrik.h"
#include <fstream>
#include <string>

VypisRejestrik::VypisRejestrik(string popis) :PolozkaMenu(popis){
}


void VypisRejestrik::provedPolozku(Hrac* hrac) {
	string line;
	ifstream in("Rejestrik.txt");
	if (in.is_open())
	{
		while (getline(in, line))
		{
			cout << line << '\n';
		}
		in.close();
	}

	else cout << "Neni mozne otevrit soubor";
}
