#include "MapaPole.h"
#include <string>

MapaPole::MapaPole() {
	Poloha* m_poloha[8] = { {(new Mesto("Mesto A",100,150)), (new Skala("Snezka")), (new Mesto("Mesto B",40,30)), (new Trava("Trava")), (new Trava("Travnik")), (new Les("Les")), (new Mesto("Mesto C",70,80)), (new Skala("Szrenica")), (new Mesto("Mesto D",130,50))}   };
}

Poloha* MapaPole::getPoloha(int poradi) {
	return m_poloha.at(poradi);
}

void MapaPole::vytvorMenu(Hrac* hrac, MapaPole* mapaPole) {
	int i, poradi;

	do {
		cout << "          ***        " << endl
			<< " Lokality ve hre: " << endl;
		for (i = 0; i<m_poloha.size(); i++) {
			cout << " " << i << " " << getPoloha(i)->getNazevPolohy() << endl;
		}
		cout<<endl << " " << i << " -> Konec " << endl;
		cout << "         ****       " << endl;
		cout << endl << "Vyber si smer : ";
		cin >> poradi;

		if (poradi < i) {
			getPoloha(poradi)->vytvorMenu(hrac, mapaPole);
		}

	} while ((poradi<0) || (poradi < i));
}
