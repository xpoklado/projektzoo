#ifndef VYPISINFOHRACE_H
#define VYPISINFOHRACE_H
#include <iostream>
#include "PolozkaMenu.h"
#include "Hrac.h"
#include "Inventar.h"
#include "Zbozi.h"

class VypisInfoHrace :public PolozkaMenu {


public:
	VypisInfoHrace(string popis);
	void provedPolozku(Hrac * hrac, Inventar * inventar, Zbozi * zbozi);
};
#endif