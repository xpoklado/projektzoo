#include "Trava.h"

Trava::Trava(string nazevPolohy): Poloha(nazevPolohy){
}

string Trava::getNazevPolohy() {
	return m_nazevPolohy;
}

Hrac* Trava::getHrace() {
	return m_hrac;
}

void Trava::setHrac(Hrac* hrac) {
	m_hrac=hrac;
}

void Trava::printInfo() {
	cout << "Pokud budete ji jit budou se Vam pridavat sily. Ted vase sila je "<< getHrace()->getSila() << endl;
}

void Trava::vytvorMenu(Hrac* hrac, MapaPole* mapaPole) {
	getHrace()->setSila(hrac->getSila() + 2);

	cout << "Vy jdete travou. Muzete tu: ";
	cout << "1. Dozvedet info o lokaci\n";
	cout << "2. Zvolit si dalsi lokace\n";
	cout << "Vase volba: ";
	int volba;
	cin >> volba;
	switch (volba) {
	case 1:
		printInfo();
	case 2:
		mapaPole->vytvorMenu(hrac, mapaPole);
}
