#ifndef ZVOLSICESTU_H
#define ZVOLSICESTU_H
#include <iostream>
#include "PolozkaMenu.h"
#include "Hrac.h"
#include "MapaPole.h"
using namespace std;

class ZvolSiCestu :public PolozkaMenu {


public:
	ZvolSiCestu(string popis);
	void provedPolozku(Hrac * hrac, MapaPole * mapaPole);
};
#endif 