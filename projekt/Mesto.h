#ifndef MESTO_H
#define MESTO_H
#include <iostream>
#include <vector>
#include "Poloha.h"
#include "Hrac.h"
#include "Zbozi.h"
#include "MapaPole.h"

using namespace std;

class Mesto :public Poloha {

private:
	vector<Zbozi*> m_zbozi;
	int m_cenaN;
	int m_cenaP;

public:
	Mesto(string nazevPolohy, int cenaN, int cenaP);
	Hrac* getHrace();
	string getNazevPolohy();
	int getCenaN();
	int getCenaP();
	void setHrac(Hrac* hrac);
	void odeberZbozi(int poradi);
	void prodejZbozi(int poradi);
	void pridejZbozi(Zbozi * zbozi);
	void nakupZbozi(Zbozi * zbozi);
	void printInfo();
	void vytvorMenuM(Hrac * hrac, MapaPole* mapaPole);
};
#endif