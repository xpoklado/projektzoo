#include "Inventar.h"
#include <string>

Inventar::Inventar(int maxPocetPredmetu) {
	m_maxPocetPredmetu = maxPocetPredmetu;
}

int Inventar::getMaxPocetPredmetu() {
	return m_maxPocetPredmetu;
}

int Inventar::getPocetZbozi() {
	return m_zbozi.size();
}

Zbozi* Inventar::getZbozi(int poradi) {
	return m_zbozi.at(poradi);
}

void Inventar::pridejZbozi(Zbozi* zbozi) {
	if (m_zbozi.size() < m_maxPocetPredmetu) {
		m_zbozi.push_back(zbozi);
	}
	else {
		cout << "Neni dostatecne mista" << endl;
	};
}

void Inventar::odeberZbozi(int poradi) {
	m_zbozi.erase(m_zbozi.at[poradi]);
}

void Inventar::printInfo() {
	cout << "Inventar obsahuje " << m_maxPocetPredmetu << " mista pro zbozi. Tu je(jsou) " << m_zbozi.size() << " zbozi." << endl;
}
